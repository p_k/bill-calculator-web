'use strict';
const mysql = require('./mysql/mysql');
const app = require('./app');

const PORT = process.env.PORT || 8100;

app.listen(PORT, () => {
  console.log(`App init on Port ${PORT}!`);
});