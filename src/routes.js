import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './components/App';
import NotFound from './components/NotFound';

const Routes = () => (
    <div>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/notFound" component={NotFound} />
            </Switch>
        </BrowserRouter>
    </div>
); 

export default Routes;