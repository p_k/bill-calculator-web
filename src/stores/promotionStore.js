import LocalStorage from './../constant/local'
import Contant from './../constant/constant'
import Dispatcher from './../dispatcher/dispatcher'
import { EventEmitter } from 'events';

const defaultPromotion = [
    {
        name : "Discount 15%",
        code : "LUCKY ONE",
        type : "moreThen",
        val1 : 1000,
        val2 : 15
    },
    {
        name : "Come 4 pay 3",
        code : "4PAY3",
        type : "customer",
        val1 : 4,
        val2 : Number(Contant.buffetPrie) * 2
    },
    {
        name : "Discount 20%",
        code : "LUCKY TWO",
        type : "customer",
        val1 : 2,
        val2 : (Number(Contant.buffetPrie) * 2) - ((Number(Contant.buffetPrie) * 2) * 20 / 100)
    },
    {
        name : "Discount 25%",
        code : "ALL",
        type : "moreThen",
        val1 : 6000,
        val2 : 25
    }
];


class PromotionStore extends EventEmitter {
    constructor() {
        super();
        this.event = new EventEmitter();
        this.objPromotion = [];
        if(
            typeof LocalStorage.getItem('promotion') !== 'undefined' && 
            LocalStorage.getItem('promotion') !== null
        ) {
            this.objPromotion = JSON.parse(LocalStorage.getItem('promotion'));
        } else {
            this.objPromotion = defaultPromotion;
        }
        this.listenerAction = this.listenerAction.bind(this);
        this.addChangeListener = this.addChangeListener.bind(this);
        this.subscribe = Dispatcher.register(this.listenerAction);
    }

    listenerAction(cb) {
        switch(cb.actionType) {
            case Contant.ADD_PM : 
                this.addPromotion(cb.item.data);
            break;
            case Contant.EDIT_PM : 
                this.editPromotion(cb.item.data, cb.item.index);
            break;
            case Contant.DEL_PM : 
                this.delPromotion(cb.item.index);
            break;
            default:
            break
        }
        LocalStorage.setItem("promotion", JSON.stringify(this.objPromotion));
        this.event.emit(Contant.CHANGE);
    }

    addPromotion(data) {
        this.objPromotion.push(data);
    }

    editPromotion(data, index) {
        this.objPromotion[index] = data;
    }
    

    addChangeListener(callback) {
        this.event.on(Contant.CHANGE, callback);
    }

    delPromotion(index) {
        this.objPromotion.splice(index, 1);
    }

    getPromotion (index = "all") {
        if(index === "all") {
            return this.objPromotion;
        } else {
            return this.objPromotion[index];
        }
    }
}

export default new PromotionStore()