import LocalStorage from './../constant/local'
import Contant from './../constant/constant'
import Dispatcher from './../dispatcher/dispatcher'
import { EventEmitter } from 'events';
import Mock from './seats'
import PromotionStore from './promotionStore'

class MainStore extends EventEmitter {
    constructor() {
        super();
        this.event = new EventEmitter();
        this.seatData = Mock.seats;
        this.queue = [];
    }

    freezObject = (obj) => { // immutable object
        return typeof obj === 'object' ? JSON.parse(JSON.stringify(obj)) : {};
    }

    addQueue = (obj) => {
        this.queue.push(obj);
    }

    caculatorBill = (person) => {
        let response = {
            conpon : 0,
            conponCode : 0,
            price : 0,
            status : false
        };
        let promotion = PromotionStore.getPromotion();
        promotion.map(obj => {
            let cbData = this.formular(obj, person);
            if((cbData.price <= response.price || response.price === 0) && cbData.status) {
                response = cbData;
            }
        })
        return response;
    }

    formular = (promotion, person) => {
        let response = {
            conpon : 0,
            conponCode : 0,
            price : 0,
            status : false
        };
        if(promotion.type === 'moreThen') {
            let value = person * Contant.buffetPrie;

            if(value >= promotion.val1) {
                response.price = value - (value * promotion.val2) / 100
                response.status = true;
                response.conpon = 1;
                response.conponCode = promotion.code;
            }
        } else if(promotion.type === 'customer') {
            let value = 0;
            let v = person;
            for(let i = 0; i < person ; i += promotion.val1) {
                if((i - promotion.val1) > promotion.val1) {
                    person -= promotion.val1;
                    response.conpon ++;
                    value += promotion.val2;
                }
            }
            response.conponCode = promotion.code;
            response.price = value + (person * Contant.buffetPrie)
            response.status = value > 0;
        }
        return response;
    }

    addChangeListener(callback) {
        this.event.on(Contant.CHANGE, callback);
    }

    getQueue() {
        return this.queue;
    }

    clearQueue(index) {
        this.queue[index].active = false;
        this.queue[index].refData.map((obj) => {
            let tableList = this.seatData.filter(objS => objS.zone === obj.zone)
            if(tableList.length > 0) { tableList = tableList[0]};
            tableList.data[obj.table].value = 0;
            tableList.data[obj.table].checked = false;
            tableList.data[obj.table].disabled = false;
        })
        this.event.emit(Contant.CHANGE);
    }

    getDataSeat() {
        return this.freezObject(this.seatData.map(obj => {
            obj.data.map(subOb => {
                subOb.disabled = subOb.checked === true
                return subOb;
            })
            return obj;
        }));
    }

    setDataSeat(obj) {
        let newQueue = [];
        let countData = 0;
        obj.map((obj, index) => {
            obj.data.map((subOb, i) => {
                if(this.seatData[index].data[i].checked !== subOb.checked) {
                    newQueue.push({
                        zone : obj.zone,
                        table : i,
                        value : subOb.value
                    });
                    countData += subOb.value;
                }
            })
        });


        this.addQueue({
            count : countData,
            active : true,
            refData : newQueue
        });
        this.seatData = obj;
        this.event.emit(Contant.CHANGE);
    }
    
} 

export default new MainStore()