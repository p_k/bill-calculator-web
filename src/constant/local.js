export default {
    setItem : function(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (100000*24*60*60*1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + escape(cvalue) + "; " + expires;
    },
  
    getItem : function(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return unescape(c.substring(name.length, c.length));
          }
      }
      return null;
    },
  
    clearStore : function() {
      var cookies = document.cookie.split(";");
  
      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
    },
  
    truncate: function(s,ld,lt,lm) {
      var l = 0;
      if(s && ld){ 
        if(window.innerWidth < 768){
          l = lm || ld;
        }else if(window.innerWidth < 992 && window.innerWidth >= 768){
          l = lt || ld;
        }else{
          l = ld;
        }
        if (s.length > l){
          return (s.slice(0,l) + "...");
        }else{
          return s;
        }
      }else{
        return
      }
    }

    // localSet : function(name, val) {
    //   localStorage.setItem(name, JSON.stringify(val));
    // }
  
}
  