import React, { Component } from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import './style.css'
import SwipeableViews from 'react-swipeable-views';

import Promotion from './promotion/index'
import Calculator from './calculator/index'
 
const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};

class App extends Component {

  constructor() {
    super();
    this.state = {
      slideIndex: 0,
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  }; 

  render() {
    return (
      <div className="App">
        <Tabs 
        onChange={this.handleChange}
        value={this.state.slideIndex}
        >
          <Tab label="Calculator" value={0} />
          <Tab label="Promotion" value={1} />
        </Tabs>
        <SwipeableViews
        index={this.state.slideIndex}
        onChangeIndex={this.handleChange}
        >
          <div style={{padding : "1rem"}}>
            <Calculator />
          </div>
          <div style={styles.slide}>
            <Promotion test={"test"} />
          </div>
        </SwipeableViews>
      </div>
    );
  }
}

export default App;
