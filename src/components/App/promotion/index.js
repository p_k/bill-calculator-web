import React, { Component } from 'react';
import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';
import ContentAdd from 'material-ui/svg-icons/content/add';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'
import FloatingActionButton from 'material-ui/FloatingActionButton';  
import ActionDelete from 'material-ui/svg-icons/action/delete'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import './style.css'
import Modal from './modal/index'
import PromotionStore from './../../../stores/promotionStore'
import PromotionAction from './../../../action/promotionAction'


class Promotion extends Component {

  constructor(props) {
    super(props);
    this.state = {
        showCheckboxes: false,
        open: false,
        selectType : "customer",
        action : "Add",
        data : [],
        index : undefined,
        alertOpen : false,
        delIndex : undefined
      };
     PromotionStore.addChangeListener(this.listenerEvent);
    }

    listenerEvent = () => {
        this.setState({
            data : PromotionStore.getPromotion(),
            open : false,
            alertOpen : false
        });
    }

    alertHandleOpen = (index) => {
        this.setState({
            alertOpen: true,
            delIndex : index
        });
    };

    alertHandleClose = () => {
        this.setState({alertOpen: false});
    };

    shouldComponentUpdate(nextProps, nextState) {
        return this.state !== nextState
    }
    
    componentDidMount() {
        this.setState({
            data : PromotionStore.getPromotion()
        });
    }

    delete(index) {
        PromotionAction.delPromotion(index);
    }

    handleOpen = (action = "Add", index = undefined) => {
        this.setState({
            open: true,
            action : action,
            index : index
        });
    };

    handleClose = () => {
        this.setState({open: false});
    };



  render() {

    const alertActions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.alertHandleClose}
        />,
        <FlatButton
          label="Delete"
          primary={true}
          onClick={() => {this.delete(this.state.delIndex)}}
        />,
      ];

    const tableData = [
        {
          name: 'Discount 15%',
          code: 'LUCKY ONE',
        }
      ];

      const options = {
        fixedHeader: true,
        fixedFooter: false,
        stripedRows: false,
        showRowHover: false,
        selectable: false,
        multiSelectable: false,
        enableSelectAll: false,
        deselectOnClickaway: false
      }

      const btnSty = {
          marginLeft : "2px",
          marginRight : "2px"
      }

      

      
    return (
      <div className="Promotion">
          <h2>Promotion Table</h2>
        <Dialog
            actions={alertActions}
            modal={false}
            open={this.state.alertOpen}
            onRequestClose={this.alertHandleClose}
        >
            Delete Promotion?
        </Dialog>
        <Modal 
            open={this.state.open} 
            action={this.state.action} 
            index={this.state.index}
            handleCloseMD={this.handleClose}
        />
        <div>
        <Table
          {...options}
        >
          <TableHeader
            displaySelectAll={this.state.showCheckboxes}
            adjustForCheckbox={this.state.showCheckboxes}
          >
            <TableRow>
              <TableHeaderColumn colSpan="3" style={{textAlign: 'center'}}>
                <div className="pm-card">
                    <div className="pm-add">
                        <label>
                        <FloatingActionButton onClick={() => {this.handleOpen()}} mini={true}>
                            <ContentAdd />
                        </FloatingActionButton>
                        <h3>Add Promotion</h3>
                        </label>
                    </div>
                </div>
            
              </TableHeaderColumn>
            </TableRow>
            <TableRow>
                <TableHeaderColumn tooltip="The Name">Action</TableHeaderColumn>
                <TableHeaderColumn tooltip="The Name">Name</TableHeaderColumn>
                <TableHeaderColumn tooltip="The Status">Code</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={this.state.showCheckboxes}
          >
            {this.state.data.map( (row, index) => (
              <TableRow key={index}>
                <TableRowColumn>
                    <FloatingActionButton onClick={() => {this.handleOpen('Edit', index)}} style={btnSty} backgroundColor={"rgba(226, 203, 0, 0.59)"} mini={true}>
                        <EditorModeEdit />
                    </FloatingActionButton>
                    <FloatingActionButton onClick={()=> {this.alertHandleOpen(index)}} style={btnSty} backgroundColor={"rgba(244, 67, 54, 0.78)"} mini={true}>
                        <ActionDelete />
                    </FloatingActionButton>
                </TableRowColumn>
                <TableRowColumn>{row.name}</TableRowColumn>
                <TableRowColumn>{row.code}</TableRowColumn>
              </TableRow>
              ))}
          </TableBody>
        </Table>
        </div>
      </div>
    );
  }
}

export default Promotion;
