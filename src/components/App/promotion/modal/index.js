import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import PromotionStore from './../../../../stores/promotionStore';
import PromotionAction from './../../../../action/promotionAction';

import reactTriggerChange from 'react-trigger-change'

export default class Modal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showCheckboxes: false,
            open: props.open,
            action : props.action,
            data : {},
            errorAlert : {
                name : false,
                code : false,
                val1 : false,
                val2 : false
            },
            component: {
                ele1 : undefined,
                ele2 : undefined,
                ele3 : undefined,
                ele4 : undefined
            }
        };
        this.handleClose.bind(this);
    }

    handleClose() {
        this.props.handleCloseMD();
    }

    handleSubmit = () => {
        reactTriggerChange(this.state.component.ele1.input);
        reactTriggerChange(this.state.component.ele2.input);
        reactTriggerChange(this.state.component.ele3.input);
        reactTriggerChange(this.state.component.ele4.input);
        
        if(
            !this.state.errorAlert.name &&
            !this.state.errorAlert.code &&
            !this.state.errorAlert.val1 &&
            !this.state.errorAlert.val2
        ) { 
            if(this.state.action === 'Add') {
                PromotionAction.addPromotion(this.state.data);
            }  
            else if (this.state.action === 'Edit') {
                PromotionAction.editPromotion(this.state.data, this.state.index);
            }
            this.props.handleCloseMD();
        }
    }

    componentWillReceiveProps(nextProps) { 
        this.setState({
            open : nextProps.open,
            action : nextProps.action,
            index : nextProps.index,
            data : (typeof nextProps.index === 'undefined') ? { type : 'customer'} : PromotionStore.getPromotion(nextProps.index),
            errorAlert : {
                name : false,
                code : false,
                val1 : false,
                val2 : false
            }
        });
    }

    handleType = (type) => {
        this.state.data.type = type;
        this.setState({ data : this.state.data});
    }

    render() {
        const checkState = (e, errorAlert) => {
            this.state.data[errorAlert] = e;
            this.state.errorAlert[errorAlert] = e === '';
            this.setState({
                errorAlert : this.state.errorAlert,
                data : this.state.data
            })
        }

        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={() => {this.handleClose()}}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={this.handleSubmit}
            />,
          ];

        return (
            <div>
                <Dialog
                title={`${this.state.action} Promotion`}
                actions={actions}
                modal={true}
                open={this.state.open}>
                <TextField
                onChange={(e) => checkState(e.target.value, 'name')}
                errorText={this.state.errorAlert.name ? "require" : ""}
                floatingLabelText="Name"
                value={this.state.data.name}
                floatingLabelFixed={true}
                ref={(TextField) => { this.state.component.ele1 = TextField; }}
                /><br />
                <TextField
                onChange={(e) => checkState(e.target.value, 'code')}
                errorText={this.state.errorAlert.code ? "require" : ""}
                floatingLabelText="Code"
                value={this.state.data.code}
                floatingLabelFixed={true}
                ref={(input) => { this.state.component.ele2 = input; }}
                /><br />
                <div>
                    <RaisedButton 
                    label="per Customers" 
                    primary={typeof this.state.data.type === 'undefined' ? true : this.state.data.type === "customer"} 
                    onClick={() => {this.handleType('customer')}}
                    />
                    <RaisedButton 
                    label="More then bill" 
                    primary={this.state.data.type === "moreThen"} 
                    onClick={() => {this.handleType('moreThen')}}
                    />
                    <br />
                </div>
    
                { (this.state.selectType === "customer") ? 
                    (<div>
                        <TextField
                        onChange={(e) => checkState(e.target.value, 'val1')}
                        errorText={this.state.errorAlert.val1 ? "require" : ""}
                        floatingLabelText="For customer"
                        value={this.state.data.val1}
                        floatingLabelFixed={true}
                        ref={(input) => { this.state.component.ele3 = input; }}
                        /><br />
                        <TextField
                        onChange={(e) => checkState(e.target.value, 'val2')}
                        errorText={this.state.errorAlert.val2 ? "require" : ""}
                        floatingLabelText="Price"
                        value={this.state.data.val2}
                        floatingLabelFixed={true}
                        ref={(input) => { this.state.component.ele4 = input; }}
                        /><br />
                    </div>) 
                    :
                        (<div>
                        <TextField
                        onChange={(e) => checkState(e.target.value, 'val1')}
                        errorText={this.state.errorAlert.val1 ? "require" : ""}
                        floatingLabelText="more then"
                        value={this.state.data.val1}
                        floatingLabelFixed={true}
                        ref={(input) => { this.state.component.ele3 = input; }}
                        /><br />
                        <TextField
                        onChange={(e) => checkState(e.target.value, 'val2')}
                        errorText={this.state.errorAlert.val2 ? "require" : ""}
                        floatingLabelText="Discount"
                        value={this.state.data.val2}
                        floatingLabelFixed={true}
                        ref={(input) => { this.state.component.ele4 = input; }}
                        /><br />
                    </div>) 
                }
            </Dialog>
          </div>
        )
    }
}