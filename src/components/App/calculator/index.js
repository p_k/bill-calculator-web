import React, { Component } from 'react';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Contant from './../../../constant/constant'
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import { grey400, darkBlack, lightBlack } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import MainStore from './../../../stores/mainStore';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import './style.css';
import Snackbar from 'material-ui/Snackbar';

class Calculator extends Component {
    constructor() {
        super();
        this.state = {
            data : MainStore.getDataSeat(),
            countSelected : 0,
            queue : MainStore.getQueue(),
            open : false,
            modal : false,
            billTxt : '',
            alert : '',
            billSelect : 0
        };
        MainStore.addChangeListener(this.listenerEvent);
    }

    listenerEvent = () => {
        this.setState({
            data : MainStore.getDataSeat(),
            queue : MainStore.getQueue(),
            countSelected : 0
        });
    }

    handleTouchTap = (txt) => {
        this.setState({
          open: true,
          alert : txt
        });
    };
    handleModalOpen = (txt, billSelect) => {
        this.setState({
            modal: true,
            billTxt : txt,
            billSelect : billSelect
        });
      };
    
    handleModalClose = () => {
        this.setState({modal: false});
        this.onClearQueue(this.state.billSelect);
    };
    
    handleRequestClose = () => {
        this.setState({
            open: false
        });
    };

    validFrom = () => {
        let status = true;
        this.state.data.map(obj => obj.data.map(subOb => { 
            if(subOb.checked && (isNaN(parseInt(subOb.value)) || parseInt(subOb.value) == 0)) {
                status = false;
            }
        }) )
        return status;
    }

    onBook = () => {
        if(this.state.countSelected > 0 && this.validFrom()) {
            MainStore.setDataSeat(this.state.data);
        }
        else if(!this.validFrom()) {
            this.handleTouchTap('มีรายการไม่สมบูรณ์');
        } else  {
            this.handleTouchTap('กรุณาเลือกรายการก่อนทำการจองคิว');
        }
    }


   handleSelected = (obj) => {
        obj.checked = obj.checked === false;
        if(obj.checked === true) { this.state.countSelected++;  }
        else { this.state.countSelected--; }
        this.setState({
            data : this.state.data
        });
    } 

    onTextChang = (obj, e, n) => {
        let number = Number(parseInt(n));
        if(parseInt(obj.max) >= number && !isNaN(number)) {
            obj.value = number;
        } else {
            obj.value = '';
        }
        this.setState({
            data : this.state.data
        });
    }

    tableComponent = (props) => { 
        return (
            <div>
                {
                    props.data.map((obj,i) => {
                        return (
                            <ListItem  key={`${obj.zone}${i}`} 
                            primaryText={` ${obj.name} ${i + 1} ( for ${obj.max} person )`} 
                            leftCheckbox={<Checkbox disabled={obj.disabled} onClick={() => {this.handleSelected(obj)}} checked={obj.checked} />} 
                            children={(obj.checked ? <TextField type="number" 
                            disabled={obj.disabled}
                            value={obj.value}
                            onChange={($e, n) => {this.onTextChang(obj, $e, n)}} 
                            hintText="Number Customers"/> : null )} />
                        )
                    })
                }
            </div>
        )
    }

    onBill = (index, person) => {
        let data = (MainStore.caculatorBill(person));
        let txt = (
            <div>
            <ListItem>
            {`Queue ${index}`}
            </ListItem>
            <ListItem>
             {`Customer : ${person}`}
             </ListItem>
             <ListItem>
             {`Default Price : ${(person * Contant.buffetPrie).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} Bath.`}
             </ListItem>
             { data.status === true ?
                    <ListItem>
                    {`Conpon Discount : x ${data.conpon} ${data.conponCode}`}
                    </ListItem>
                : null
             }
            <ListItem>
                {`Net amount  : ${ (data.status) ? 
                (data.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")) : 
                (person * Contant.buffetPrie).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") } Bath. `}
            </ListItem>
             </div>

        );
        this.handleModalOpen(txt, index);
    }

    onClearQueue = (index) => {
        MainStore.clearQueue(index);
    }

    render() {
        const iconButtonElement = (
            <IconButton
              touch={true}
              tooltip="more"
              tooltipPosition="bottom-left"
            >
              <MoreVertIcon color={grey400} />
            </IconButton>
          );

        const style = {

        }

        const rightIconMenu = (index, count) =>{
            return (
                <IconMenu iconButtonElement={iconButtonElement}>
                <MenuItem onClick={() => {this.onBill(index, count)}}>Bill</MenuItem>
                <MenuItem onClick={() => {this.onClearQueue(index)}}>Delete</MenuItem>
                </IconMenu>
            )
        };   

        const actions = [
            <FlatButton
              label="Bill"
              primary={true}
              keyboardFocused={true}
              onClick={this.handleModalClose}
            />
          ];
        return (
            <div>
                <Dialog
                title="Restaurant"
                actions={actions}
                modal={false}
                open={this.state.modal}
                onRequestClose={this.handleModalClose}
                >
                {this.state.billTxt}
                </Dialog>
                <Snackbar
                    open={this.state.open}
                    message={this.state.alert}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestClose}
                />
            <h2>Restaurant</h2>
            <div className="calculator-card">
                <div className="queue-card">
                    <List>
                        <Subheader> <h3>Queue Lists</h3></Subheader>
                        {this.state.queue.map((obj, i) => {
                                return (
                                    <div>
                                        { obj.active ? 
                                              <div>

                                              <ListItem
                                                 rightIconButton={rightIconMenu(i, obj.count)}
                                                 primaryText={`Queq Number : ${i} `}
                                                 />
                                                 {
                                                 obj.refData.map(subOb=> {
                                                    return (    
                                                        <ListItem>
                                                             <blockquote>
                                                             {`Zone ${subOb.zone} Table ${Number(subOb.table) + 1} ${Number(subOb.value)} Person `}
                                                             </blockquote>
                                                        </ListItem>
                                                        )
                                                    })
                                                    }
                                                 <Divider inset={true} />
                                         </div>
                                           : null }
                                    </div>
            
                                )
                        })}
                    </List>
                </div>
                    <div className="seats-card">
                        <div class="headerTable">
                            <Subheader> <h3>Seats Lists</h3> </Subheader>
                            <RaisedButton onClick={this.onBook}  label="BOOK" primary={true} />
                        </div>
                        {
                            this.state.data.map((obj,i) => {
                            return (
                                <List key={obj.zone}>
                                <Subheader>Zone {obj.zone}</Subheader>
                                    {this.tableComponent(obj)}
                                </List>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }

}

export default Calculator;