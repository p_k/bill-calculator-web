import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Routes from './routes';
require('./style.css')
   
const App = () => (
  <div className="App">
    <MuiThemeProvider>
      <Routes />
    </MuiThemeProvider>
  </div>
);

ReactDOM.render(
    <App/>,
    document.getElementById('root')
  );
