import Dispatcher from '../dispatcher/dispatcher'
import Constants from '../constant/constant'

class PromotionAction {
    addPromotion(obj) {
        Dispatcher.dispatch({
            actionType : Constants.ADD_PM,
            item : {
                data : obj
            }
          });
    }

    editPromotion(obj, index) {
        Dispatcher.dispatch({
            actionType : Constants.EDIT_PM,
            item : {
                data : obj,
                index : index
            }
        });
    }


    delPromotion(index) {
        Dispatcher.dispatch({
            actionType : Constants.DEL_PM,
            item : {
                index : index
            }
        });
    }
}

export default new PromotionAction()

